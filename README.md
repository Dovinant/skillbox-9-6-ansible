# Практическая работа 9.6

В работе с помощью Terraform разворачивается инфраструктура в облаке Яндекс: однин серевер с ReactJS, два серевера с Nginx в режиме reverse proxy и один балансировщик. Балансировщик принимает запросы от пользователя, передаёт серверам Nginx, они запращивают приложение на серевере ReactJS.

По заданию, на всех серверах должен быть предустановлен nginx. Это делает Terraform, смотри файл terraform/meta.yml, строка 35. Дальнейшая настройка серверов выполнена через Ansible в Task01 и Task02. Разница между ними в том, что в Task02 применяются роли Ansible, их там две: nginx и reactapp.
Таким образом, файл Task01/ansible/reactjs_playbook.yml разделён в Task02 на три: файлы deployapp.yml и setup.yml в каталоге Task02/ansible/roles/reactapp/tasks, также в файл main.yml в каталоге Task02/ansible/roles/reactapp/defaults вынесены переменные.

Файл Task01/ansible/nginx.yml также разделён на три части в каталоге Task02/ansible/roles/nginx/: tasks/reverse_proxy.yml, handlers/main.yml, defaults/main.yml. Файл Task01/ansible/nginx.conf.j2 перенесён в каталог Task02/ansible/roles/nginx/templates.

Кроме того, в Task02 из файла ansible/hosts удалены переменные ansible_user и ansible_ssh_private_key_file. Они помещены в файлы group_vars/nginx_servers.yml и group_vars/reactjs_servers.yml (можно было бы поместить в один файл group_vars/all.yml).
Сам файл Task02/ansible/hosts генерируется Terraform - смотри секцию __---- Generate inventory file ----__ в Task02/terraform/main.tf, строка 153.

По заданию в Task02, IP address серевера ReactJS должен подставлятся в файл конфигурации для Nginx из переменной hostvars.reactjs_1.ansible_default_ipv4.address. Я это сделал в файле Task02/ansible/roles/nginx/defaults/main.yml, но можно было бы прямо указать её в файле Task02/ansible/roles/nginx/templates/nginx.conf.j2, например так:
> `proxy_pass http://{{ hostvars['reactjs_1'].ansible_default_ipv4.address }}:{{ reactjs_host_port }};`

## Сложности

### Модули replace и lineinfile - в чём разница?
Модули [replace](https://www.educba.com/ansible-replace/) и [lineinfile](https://www.educba.com/ansible-lineinfile/) изменяют указанный файл по определённому образцу. Здесь они работают в файле Task01/ansible/reactjs_playbook.yml. Но replace заменит все строки совпадающие с критерием поиска, а lineinfile только одну (по умолчанию последнюю из найденных).
### pipe lookup plugin
[pipe](https://stackoverflow.com/questions/39554281/ansible-lookup-pipe-what-does-this-pipe-means) это ansible lookup plugin. Возвращает результат работы переданной ему команды. Смотри строку 64 файла Task01/ansible/reactjs_playbook.yml. [Подробнее...](https://docs.ansible.com/ansible/latest/plugins/lookup.html)
### FQDN для балансировщика
Похоже в Yandex Cloud нельзя сгенерировать доменное имя как в AWS. Для этого надо отдельно зарегистрировать домен и в качестве NS серверов указать серевера Яндекс, а потом вручную сооздавать A-records. [Подробнее...](https://cloud.yandex.ru/docs/dns/concepts/compute-integration)

## Дополнительные материалы

### Ansible
- [Пособие по Ansible](https://habr.com/ru/post/305400/)
- [Про Ansible для новичков: Практика (часть II)](https://habr.com/ru/company/nixys/blog/674088/)
- [Основы Ansible, без которых ваши плейбуки — комок слипшихся макарон](https://habr.com/ru/post/508762/)
- [Автоматизируем и ускоряем процесс настройки облачных серверов с Ansible. Часть 5: local_action, условия, циклы и роли](https://habr.com/ru/company/infobox/blog/252461/)
- [Основы Ansible 2.9 для сетевых инженеров. Роли.](https://ansible-for-network-engineers.readthedocs.io/ru/latest/book/06_playbooks/roles.html)
- [Ansible Lookup](https://www.educba.com/ansible-lookup/)

### Terraform
- [Terraform за 15 дней (AWS/Yandex cloud)](https://habr.com/ru/post/684964/)

### Yandex Cloud
- [Отказоустойчивый сайт с балансировкой нагрузки с помощью Yandex Network Load Balancer](https://cloud.yandex.ru/docs/tutorials/web/load-balancer-website)
- [Имя хоста и внутренний FQDN](https://cloud.yandex.ru/docs/compute/concepts/network#hostname)

### Nginx
- [Как настроить Nginx в качестве балансировщика нагрузки](https://habr.com/ru/company/first/blog/683870/?ysclid=laaoudmw81774386814)

### Jinja2
- [Уроки по Jinja2 (Youtube)](https://www.youtube.com/playlist?list=PLA0M1Bcd0w8wfmtElObQrBbZjY6XeA06U)
